class RecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      date: '',
      amount: ''
    };
    this.handleChange = e => {
      let name = e.target.name;
      this.setState({[name]: e.target.value});
    };
    this.valid = () => {
      return this.state.title && this.state.date && this.state.amount;
    };
    this.handleSubmit = e => {
      e.preventDefault()
      $.post("", { record: this.state }, data => {
        this.props.handleNewRecord(data);
        this.setState({
          title: '',
          date: '',
          amount: ''
        });
      }, "json");
    }
  }
  render() {
    return (
      <form className="form-inline" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input type="text" className="form-control" placeholder="Date" name="date" value={this.state.date} onChange={this.handleChange} />
        </div>
        <div className="form-group">
          <input type="text" className="form-control" placeholder="Title" name="title" value={this.state.title} onChange={this.handleChange} />
        </div>
        <div className="form-group">
          <input type="number" className="form-control" placeholder="Amount" name="amount" value={this.state.amount} onChange={this.handleChange} />
        </div>
        <input type="submit" className="btn btn-primary" disabled={!this.valid()} value="Create record" />
      </form>
    );
  } 
}