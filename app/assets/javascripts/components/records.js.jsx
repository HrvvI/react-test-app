class Records extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      records: this.props.data
    };
    this.addRecord = record => {
      let records = React.addons.update(this.state.records, { $push: [record] });
      this.setState({records: records});
    };
    this.deleteRecord = record => {
      let index = this.state.records.indexOf(record);
      let records = React.addons.update(this.state.records, { $splice: [[index, 1]] });
      this.setState({records: records});
    };
    this.updateRecord = (record, data) => {
      let index = this.state.records.indexOf(record);
      let records = React.addons.update(this.state.records, { $splice: [[index, 1, data]] });
      this.setState({records: records});
    };
    this.credits = () => {
      let credits = this.state.records.filter( val => val.amount > 0 );
      return credits.reduce((acc, x) => {
        return acc + parseFloat(x.amount);
      }, 0);
    };
    this.debits = () => {
      let debits = this.state.records.filter( val => val.amount < 0 );
      return debits.reduce((acc, x) => {
        return acc + parseFloat(x.amount);
      }, 0);
    };
    this.balance = () => {
      return this.debits() + this.credits();
    };
  }
  render() {
    return (
      <div className="records">
        <h2 className="title"> Records </h2>
        <div className="row">
          <AmountBox type="success" amount={this.credits()} text="Credit" />
          <AmountBox type="danger" amount={this.debits()} text="Debit" />
          <AmountBox type="info" amount={this.balance()} text="Balance" />
        </div>
        <RecordForm handleNewRecord={this.addRecord} />
        <hr />
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Date</th>
              <th>Title</th>
              <th>Amount</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.records.map( record => {
              return <Record key={record.id} record={record} handleDeleteRecord={this.deleteRecord} handleEditRecord={this.updateRecord}/>;
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

Records.defaultProps = {
  records: []
};